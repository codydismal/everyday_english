import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    serverURL: ''
  },
  mutations: {
    setServerURL (state, url) {
        state.serverURL = url;
    }
  },
  actions: {

  }
})

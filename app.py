import atexit
import os

from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask, request, render_template
from flask_cors import CORS

from database import get_sqlalchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./test.db'
ACCESS_TOKEN = 'EAAduVzc17WcBAMzbYsxIoVyK3CdgCk8VlIYw5ZBnlbg6QeaOpuA1iu2tGjl569PDalwDZA7K4HZCGnfDnG2qZBF9cl9XBUvW2njPbpQZBzZAv822ElErE3mV77XESKF3HYpc96QBcwoxLQaCHZBRblkfuccDMzbQyMp0oTeKuNGZA8zK4dtgW8hEguo44ltFEhsZD'
VERIFY_TOKEN = 'fewrj32kln32klkon32dsfkjkl3jkljrklj32roi0f32i02j3f'

db = get_sqlalchemy(app)
from logic.bot import get_bot, send_message, send_button_message
from logic.admin import get_words, add_word, delete_word, parse_csv_file

bot = get_bot(ACCESS_TOKEN)

if not os.path.exists(app.config['SQLALCHEMY_DATABASE_URI']):
    db.create_all()

CORS(app)

from logic import handle_messages, get_random_word

# scheduler = BackgroundScheduler()
# scheduler.add_job(func=send_word_to_all, trigger="cron", hour=9, minute=30)
# scheduler.add_job(func=send_word_to_all, trigger="cron", hour=13, minute=30)
# scheduler.add_job(func=send_word_to_all, trigger="cron", hour=17, minute=30)
# scheduler.start()

# Shut down the scheduler when exiting the app
# atexit.register(lambda: scheduler.shutdown())


# We will receive messages that Facebook sends our bot at this endpoint
@app.route("/facebook", methods=['GET', 'POST'])
def receive_message():
    if request.method == 'GET':
        token_sent = request.args.get("hub.verify_token")
        return verify_fb_token(token_sent)
    result_message = handle_messages(request)

    print("Message to send: {}".format(result_message.text))

    if result_message.buttons:
        send_button_message(result_message.recipient_id, result_message.text, result_message.buttons)
        return "Message Processed"

    send_message(result_message.recipient_id, result_message.text)
    return "Message Processed"


@app.route("/", methods=['GET', 'POST'])
def landing():
    return render_template('landing.html')


@app.route("/admin", methods=['GET', 'POST'])
def admin():
    return render_template('index.html')


@app.route("/api/words", methods=['GET', 'POST', 'DELETE'])
def words():
    if request.method == 'GET':
        return get_words(1)
    elif request.method == 'POST':
        data = request.json
        add_word(data)
    elif request.method == 'DELETE':
        delete_word(request.args)

    return "Success"


@app.route("/api/import", methods=['POST'])
def csv_import():
    parse_csv_file(request.files)

    return "Success"


def verify_fb_token(token_sent):
    # take token sent by facebook and verify it matches the verify token you sent
    # if they match, allow the request, else return an error
    if token_sent == VERIFY_TOKEN:
        return request.args.get("hub.challenge")
    return 'Invalid verification token'


if __name__ == "__main__":
    app.run('0.0.0.0')

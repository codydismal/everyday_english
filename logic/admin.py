from sqlalchemy import func
import csv
from database import get_sqlalchemy
from database.message import Message

db = get_sqlalchemy()


def get_words(page):
    words = Message.query.order_by(Message.id.asc()).paginate(page, 10, error_out=False)
    return {
        'words': [word.as_dict() for word in words.items],
        'pages': words.pages,
        'hasNext': words.has_next,
        'hasPrev': words.has_prev
    }


def add_word(data):
    last_order = db.session.query(func.max(Message.order)).scalar()
    if last_order is None:
        last_order = 0
    last_order += 1
    word = Message(
        order=last_order,
        en_word=data['enWord'],
        pl_word=data['plWord'],
        transcription=data['transcription'],
        lexical_category=data['lexicalCategory'],
        en_example=data['enExample'],
        pl_example=data['plExample']
    )
    db.session.add(word)
    db.session.commit()


def delete_word(data):
    message = Message.query.filter_by(id=data['id']).delete()
    db.session.commit()


def parse_csv_file(files):
    file = files['file']
    csvfile = (line.decode('utf8') for line in file.readlines())
    reader = csv.DictReader(csvfile)

    last_order = db.session.query(func.max(Message.order)).scalar()
    if last_order is None:
        last_order = 0
    for data in reader:

        last_order += 1
        word = Message(
            order=last_order,
            en_word=data['English word'],
            pl_word=data['Polish word'],
            transcription=data['Transcription'],
            lexical_category=data['Lexical category'],
            en_example=data['English example'],
            pl_example=data['Polish example']
        )
        db.session.add(word)
    db.session.commit()

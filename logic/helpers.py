def get_message_text_and_recipient_id(request):
    recipient_id = None
    message_text = '!!! NIE NADANO TEKSTU !@#$%^&'
    output = request.get_json()
    for event in output.get('entry', []):
        messaging = event.get('messaging', [])
        for message in messaging:
            recipient_id = message['sender']['id']
            if message.get('message'):
                if message['message'].get('text'):
                    message_text = message['message'].get('text')
            if message.get('postback'):
                message_text = message.get('postback').get('title')
    return message_text, recipient_id

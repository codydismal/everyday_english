from pymessenger import Bot

_bot = None


def get_bot(access_token):
    global _bot
    if not _bot and access_token:
        _bot = Bot(access_token)
    return _bot


def send_message(recipient_id, response):
    global _bot
    _bot.send_text_message(recipient_id, response)
    print("Message without buttons sent.")
    return "success"


def send_button_message(recipient_id, response, buttons):
    global _bot
    _bot.send_button_message(recipient_id, response, buttons)
    print("Message with buttons sent.")

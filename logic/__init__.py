import random

from sqlalchemy import func
from werkzeug.exceptions import abort

import logic.strings as strings
from database import get_sqlalchemy
from database.message import Message
from database.user import User
from logic.bot import send_message
from logic.helpers import get_message_text_and_recipient_id

db = get_sqlalchemy()


class MessageToSend:
    def __init__(self, recipient_id, text, buttons=None):
        self.recipient_id = recipient_id
        self.text = text
        self.buttons = buttons


def get_random_word():
    # random_word_id = random.randrange(0, db.session.query(Message).count())
    # word = Message.query.filter(Message.id == random_word_id).first()
    word = Message.query.order_by(func.random()).first()

    if not word:
        return "Baza danych ze słowami jest pusta!"

    return "{0} – {1}\n{2} [{3}]\n\n{4}\n{5}".format(word.en_word,
                                                     word.pl_word,
                                                     word.lexical_category,
                                                     word.transcription,
                                                     word.en_example,
                                                     word.pl_example)


def handle_messages(request):
    message_text, recipient_id = get_message_text_and_recipient_id(request)

    actions_by_message_text = {
        'tak': register_or_find_user(recipient_id),
        'kolejne słowo': get_random_word(),
        'zaczynamy!': get_random_word()
    }

    buttons_by_message_text = {
        'tak': [{
            "type": "postback",
            "title": "Zaczynamy!",
            "payload": "null"
        }],
        'kolejne słowo':[{
            "type": "postback",
            "title": "Kolejne słowo",
            "payload": "null"
        }],
        'zaczynamy!': [{
            "type": "postback",
            "title": "Kolejne słowo",
            "payload": "null"
        }]
    }

    print(message_text)

    result_text = actions_by_message_text.get(message_text.lower(), strings.welcome_message) or strings.welcome_message
    result_buttons = buttons_by_message_text.get(message_text.lower(), []) or []

    return MessageToSend(recipient_id, result_text, result_buttons)


def register_or_find_user(recipient_id):
    user = User.query.filter(User.facebook_id == recipient_id).first()
    if user is None:
        user = User(facebook_id=recipient_id)
        db.session.add(user)
        db.session.commit()
    return strings.registration_success

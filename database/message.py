from database import get_sqlalchemy

db = get_sqlalchemy()


class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    order = db.Column(db.Integer, unique=True)
    en_word = db.Column(db.String)
    pl_word = db.Column(db.String)
    transcription = db.Column(db.String)
    lexical_category = db.Column(db.String)
    en_example = db.Column(db.String)
    pl_example = db.Column(db.String)
    posted = db.Column(db.Boolean, default=False)

    def as_dict(self):
        return {
            'id': self.id,
            'order': self.order,
            'enWord': self.en_word,
            'plWord': self.pl_word,
            'transcription': self.transcription,
            'lexicalCategory': self.lexical_category,
            'enExample': self.en_example,
            'plExample': self.pl_example,
            'posted': self.posted
        }

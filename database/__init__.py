from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import sessionmaker

_db = None
_session_factory = None


def get_sqlalchemy(app=None):
    global _db, _session_factory
    if not _db and app:
        _db = SQLAlchemy(app)
        _session_factory = sessionmaker(bind=_db)
    return _db

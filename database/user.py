from database import get_sqlalchemy

db = get_sqlalchemy()


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    facebook_id = db.Column(db.Integer)
